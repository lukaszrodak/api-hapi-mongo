require = require("esm")(module, {
    mode: "auto",
    await: true,
    cjs: {
        vars: true,
        namedExports: true
    }
});

module.exports = require("./src/app.js");

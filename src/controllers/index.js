import usersController from "./usersController";
import groupsController from "./groupsController";

export default [
    ...usersController,
    ...groupsController
];

import mongoose from "mongoose";
import User from "../models/User";
import serializeError from "serialize-error";
import escapeStringRegExp from "escape-string-regexp";
import Joi from 'joi';

export default [
    //get user information by id
    {
        method: "GET",
            path: "/api/users/{id}",
            config: {
                async handler(request, reply) {
                if (!mongoose.Types.ObjectId.isValid(request.params.id)) {
                    return reply({
                        message: "Invalid id"
                    }).code(400);
                }

                let user = await User.findById(request.params.id);

                if (!user) {
                    return reply({
                        message: "User not found!"
                    }).code(404);
                }

                return reply(user.toObject());
            }
        }
    },

    //add user information
    {
        method: "POST",
        path: "/api/users",
        config: {
            payload: {
                allow: "application/json"
            }
        },
        async handler(request, reply) {
            let user = new User({name: request.payload.name, email: request.payload.email, group: request.payload.group});

            try {
                await user.validate();
            } catch (e) {
                return reply(serializeError(e)).code(400);
            }

            await user.save();
            return reply(user.toObject()).code(201);
        }
    },

    //update user information by id
    {
        method: "PATCH",
        path: "/api/users/{id}",
        config: {
            payload: {
                allow: "application/json"
            }
        },
        async handler(request, reply) {

            let user = await User.findById(request.params.id);
            if (!user) {
                return reply({
                    message: "User not found!"
                }).code(404);
            }

            user.name = request.payload.name;
            user.email = request.payload.email;

            try {
                await user.validate();
            } catch (e) {
                return reply(serializeError(e)).code(400);
            }

            await user.save();
            return reply(user.toObject());
        }
    },

    //delete user information by id
    {
        method: "DELETE",
        path: "/api/users/{id}",
        config: {
            async handler(request, reply) {

                let user = await User.findByIdAndRemove(request.params.id);
                if (!user) {
                    return reply({
                        message: "User not found!"
                    }).code(404);
                }
                return reply({message: "User deleted!"}).code(204);
            }
        }
    },

    //search user
    //ex: /api/users/search?q=email@email.com
    {
        method: "GET",
        path: "/api/users/search",
        config: {
            validate: {
                query: {
                    q: Joi.string()
                        .max(100)
                        .optional(),
                    group: Joi.string()
                        .max(100)
                        .optional()
                }
            },
            async handler(request, reply) {
                let groupCondition = {};
                let queryCondition = {};
                if(request.query.group && mongoose.Types.ObjectId.isValid(request.query.group)) {
                    groupCondition = {
                        group: new mongoose.Types.ObjectId(request.query.group)
                    };
                }
                if(request.query.q) {
                    const queryRegex = new RegExp(escapeStringRegExp(request.query.q), "i");
                    queryCondition = {
                        $or: [
                            { name: queryRegex },
                            { email: queryRegex }
                        ]
                    }
                }
                let user = await User.aggregate([
                    {
                        $match:
                            {
                                ...groupCondition,
                                ...queryCondition
                            }
                    },
                    {
                        $lookup: {
                            from: "groups",
                            localField: "group",
                            foreignField: "_id",
                            as: "group"
                        }
                    },
                    {
                        $addFields: {
                            group: { $arrayElemAt: ["$group", 0] }
                        }
                    },
                    {
                        $sort: {
                            name: 1
                        }
                    }
                ]);

                return reply(user);
            }
        }
    },

    //get all users
    {
        method: "GET",
        path: "/api/users",
        config: {
            async handler(request, reply) {

                let users = await User.aggregate([
                    {
                        $lookup: {
                            from: "groups",
                            localField: "group",
                            foreignField: "_id",
                            as: "group"
                        }
                    },
                    {
                        $addFields: {
                            group: { $arrayElemAt: ["$group", 0] }
                        }
                    },
                    {
                        $sort: {
                            name: 1
                        }
                    }
                ]);

                if (!users) {
                    return reply({
                        message: "Users not found!"
                    }).code(404);
                }

                return reply(users);
            }
        }
    }
]
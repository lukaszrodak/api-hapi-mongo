import mongoose from "mongoose";
import Group from "../models/Group";
import serializeError from "serialize-error";
import escapeStringRegExp from "escape-string-regexp";
import Joi from 'joi';

export default [
    //get group information by id
    {
        method: "GET",
        path: "/api/groups/{id}",
        config: {
            async handler(request, reply) {
                if (!mongoose.Types.ObjectId.isValid(request.params.id)) {
                    return reply({
                        message: "Invalid id"
                    }).code(400);
                }

                let group = await Group.findById(request.params.id);

                if (!group) {
                    return reply({
                        message: "User not found!"
                    }).code(404);
                }

                return reply(group.toObject());
            }
        }
    },

    //add group information
    {
        method: "POST",
        path: "/api/groups",
        config: {
            payload: {
                allow: "application/json"
            }
        },
        async handler(request, reply) {
            let group = new Group({name: request.payload.name});

            try {
                await group.validate();
            } catch (e) {
                return reply(serializeError(e)).code(400);
            }

            await group.save();
            return reply(group.toObject()).code(201);
        }
    },

    //update group information by id
    {
        method: "PATCH",
        path: "/api/groups/{id}",
        config: {
            payload: {
                allow: "application/json"
            }
        },
        async handler(request, reply) {

            let group = await Group.findById(request.params.id);
            if (!group) {
                return reply({
                    message: "User not found!"
                }).code(404);
            }

            group.name = request.payload.name;

            try {
                await group.validate();
            } catch (e) {
                return reply(serializeError(e)).code(400);
            }

            await group.save();
            return reply(group.toObject());
        }
    },

    //delete group by id
    {
        method: "DELETE",
        path: "/api/groups/{id}",
        config: {
            async handler(request, reply) {
                let users = await User.find({groupId: request.params.id});
                if (users && users.length) {
                    return reply({
                        message: "Group can not deleted! Please deleted users connected to this group and try again later."
                    }).code(422);
                }
                let group = await Group.findByIdAndRemove(request.params.id);
                if (!group) {
                    return reply({
                        message: "User not found!"
                    }).code(404);
                }
                return reply({message: "Group deleted!"}).code(204);
            }
        }
    },

    //search group
    //ex: /api/groups/search?q=Users
    {
        method: "GET",
        path: "/api/groups/search",
        config: {
            validate: {
                query: {
                    q: Joi.string()
                        .max(100)
                        .optional()
                }
            },
            async handler(request, reply) {
                const queryRegex = new RegExp(escapeStringRegExp(request.query.q), "i");
                let group = await Group.find(
                    {
                        $or: [
                            { name: queryRegex }
                        ]
                    }
                );
                return reply(group);
            }
        }
    },
]
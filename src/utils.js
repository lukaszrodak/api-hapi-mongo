import ApplicationStore from "./models/ApplicationStore";

export async function getApplicationStore() {
    let applicationStore = await ApplicationStore.findOne({}).exec();
    if (!applicationStore) {
        applicationStore = new ApplicationStore();
        await applicationStore.save();
    }
    return applicationStore;
}
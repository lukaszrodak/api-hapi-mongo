import Hapi from "hapi";
import good from "good";
import controllers from "./controllers";
import migrations from "./migrations";
import mongoose from "mongoose";
import "good-squeeze";
import "good-console";

async function run() {
    const server = new Hapi.Server();

    server.connection({
        port: 3000,
        host: "localhost",
        routes: {
            cors: true
        }
    });

    const loggingOptions = {
        ops: {
            interval: 1000
        },
        reporters: {
            myConsoleReporter: [
                {
                    module: "good-squeeze",
                    name: "Squeeze",
                    args: [{log: "*", response: "*", error: "*"}]
                },
                {
                    module: "good-console"
                },
                "stdout"
            ],
            ...({})
        }
    };


    try {
        await server.register({
            register: good,
            options: loggingOptions
        });

        await mongoose.connect("mongodb://localhost/database", {
            useNewUrlParser: true,
            reconnectTries: Number.MAX_VALUE,
            reconnectInterval: 500
        });
        server.log(["database", "success"], "Sucessfully connected to MongoDB.");

        await migrations(server);

        server.route(controllers);

        await server.start();
        server.log(["success"], `Server successfully started at: ${server.info.uri}`);

    } catch (e) {
        console.error("Server init failed", e);
        process.exit(4999);
    }
}

run();
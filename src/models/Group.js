import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

export const groupSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true
    }
});

groupSchema.plugin(uniqueValidator);
export default mongoose.model("Group", groupSchema);

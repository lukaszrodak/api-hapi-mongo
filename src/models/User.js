import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

export const userSchema = mongoose.Schema({
    email: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    group: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: "Group"
    }
});

userSchema.plugin(uniqueValidator);
export default mongoose.model("User", userSchema);

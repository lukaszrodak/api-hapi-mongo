import mongoose from "mongoose";

export const applicationStoreSchema = mongoose.Schema({
    migrationVersion: {
        type: Number,
        default: 0
    }
});
export default mongoose.model("ApplicationStore", applicationStoreSchema);

import { getApplicationStore } from "../utils";
import User from "../models/User";
import Group from "../models/Group";

//  In user schema added field "Group". The field is required from now.
//  This migration added default group to user created before modification in schema.
//  If "Default" group not exist, the migration create that.

export default async function migration1(server) {
    let appStore = await getApplicationStore();
    if (!appStore.schemaVersion || appStore.schemaVersion < 1) {
        server.log(["migration", "migration1"], "Running migration1");
        let users = await User.find({}).exec();
        let groups = await Group.findOne({name: "Default"}).exec();
        if(!groups) {
            let defaultGroup = new Group({name: "Default"});
            groups = await defaultGroup.save();
            console.log(groups);
        }
        for (let user of users) {
            let modified = false;
            if (!user.group) {
                user.group = groups._id;
                modified = true;
            }
            if (modified) {
                server.log(["migration", "migration2"], `Migrated user ${user._id}.`);
                await user.save();
            }
        }
        appStore.migrationVersion = 1;
        await appStore.save();
    }
}

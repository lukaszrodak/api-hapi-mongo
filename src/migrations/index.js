import { getApplicationStore } from "../utils";
import migration1 from "./migration1";


export default async function(server) {
    server.log(["migration", "info"], "Running migrations...");
    await migration1(server);

    server.log(["migration", "success"], "Done running migrations!");
    let appStore = await getApplicationStore();
    server.log(["migration", "info"], `Current migrationVersion: ${appStore.migrationVersion}.`);
}

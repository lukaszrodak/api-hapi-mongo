## Short description

This is simple CRUD project use Hapi.js and MongoDB. 

In this project implemented:
-models,
-controlers,
-migrations.

---

## How run it

You’ll start by downloading this code.

1. Install Node.js, MongoDB.
2. Run mongodb server.
3. Create empty database named as "database".
4. Run command: npm install.
5. Start project by command: node server.js.
6. Enjoy!